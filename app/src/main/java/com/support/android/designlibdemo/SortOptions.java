package com.support.android.designlibdemo;

/**
 * Created by jillianforde on 6/13/16.
 */

public interface SortOptions {
    void sortByPopularity(MovieSorter.NetworkListener networkListener);
    void sortByRating(MovieSorter.NetworkListener networkListener);
}
